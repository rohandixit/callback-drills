const getBoardInfo = require('./callback1');
const getListForBoardid = require('./callback2');
const getCardsForListid = require('./callback3');

// Function for Problem here
function getAllInfo(boardId) {
    setTimeout(()=>{

        // Call Function to get board Information
        getBoardInfo(boardId, (boardErr, boardData) => {

            // If there is an error
            if(boardErr){
                console.log('Its Board Error', boardErr);
            } else {

                // Call the get List Function
                getListForBoardid(boardData.id, (listErr, listData) => {

                    // If it gets an error
                    if (listErr) {
                        console.log('Its List Error', listErr)
                    } else {

                        // Find the Mind Stone thingie
                        const mindStone = listData.find(item => item.name == 'Mind');

                        // Get all the cards for Mind Stone
                        getCardsForListid(mindStone.id, (cardErr, cardData) => {

                            // If we gets an error
                            if(cardErr){
                                console.log('Its card Error', cardErr);
                            } else {
                                console.log(cardData);
                            }

                        });

                    };

                });
            }
        });
    }, 2 * 1000);
}

// Export the function
module.exports = getAllInfo;