const fs = require('fs');

// Fucntion to return list of from lists.json

function callbackFunction(boardId, callback) {

    setTimeout(() => {

        let buffer = fs.readFileSync('data/lists.json');
        let data = JSON.parse(buffer.toString());
        
        let dataToReturn = function(data){

            if (data.hasOwnProperty(boardId)) {
                return data[boardId];
            }

        }(data);

        // Call the callback function
        if (dataToReturn){
            callback(null, dataToReturn)
        } else {
            callback("Id not found", dataToReturn);
        }

    }, 2000)

};

// export the function
module.exports = callbackFunction