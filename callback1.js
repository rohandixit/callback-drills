const fs = require('fs');

// Fucntion to return board information

function board(boardId, callback) {

    setTimeout(() => {

        let buffer = fs.readFileSync('data/boards.json');
        let data = JSON.parse(buffer.toString());
        
        let dataToReturn = data.find(item => item.id == boardId);

        // Call the callback function
        if (dataToReturn){
            callback(null, dataToReturn)
        } else {
            callback("Id not found", dataToReturn);
        }

    }, 2000)

};

// export the function
module.exports = board