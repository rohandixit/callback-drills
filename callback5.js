const getBoardInfo = require('./callback1');
const getListForBoardid = require('./callback2');
const getCardsForListid = require('./callback3');

// Function for Problem here
function getAllInfo(boardId) {
    setTimeout(()=>{

        // Call Function to get board Information
        getBoardInfo(boardId, (boardErr, boardData) => {

            // If there is an error
            if(boardErr){
                console.log('Its Board Error', boardErr);
            } else {

                // Call the get List Function
                getListForBoardid(boardData.id, (listErr, listData) => {

                    // If it gets an error
                    if (listErr) {
                        console.log('Its List Error', listErr)
                    } else {

                        // Find the Mind Stone and Space stone thingie
                        const stones = listData.filter(item => item.name == 'Mind' || item.name == 'Space');

                        // Get all the cards for Mind Stone
                        for (let stone of stones) {

                            getCardsForListid(stone.id, (cardErr, cardData) => {

                                // If we gets an error
                                if(cardErr){
                                    console.log('Its card Error', cardErr);
                                } else {
                                    console.log(cardData);
                                }

                            });
                        };

                    };

                });
            }
        });
    }, 2 * 1000);
}

// Export the function
module.exports = getAllInfo;