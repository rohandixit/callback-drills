const fs = require('fs');

// Fucntion to return list of from cards.json

function callbackFunction(listId, callback) {

    setTimeout(() => {

        let buffer = fs.readFileSync('data/cards.json');
        let data = JSON.parse(buffer.toString());
        
        let dataToReturn = function(data){

            if (data.hasOwnProperty(listId)) {
                return data[listId];
            }

        }(data);

        // Call the callback function
        if (dataToReturn){
            callback(null, dataToReturn)
        } else {
            callback("Id not found", dataToReturn);
        }

    }, 2000)

};

// export the function
module.exports = callbackFunction