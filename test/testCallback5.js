const fs = require('fs');
const testFunc = require('../callback5');

// Read the Boards.json
const buffer = fs.readFileSync('data/boards.json');

// Convert buffer to json object
const boardsData = JSON.parse(buffer.toString());

// Find the Board with thanos and call the test Function
const board = boardsData.find(item => item.name == 'Thanos');

// test the test function
testFunc(board.id);