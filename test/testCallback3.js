// Import the funciton
const callbackFunction = require('../callback3')

// Test the function using a callback function
callbackFunction('qwsa221', (err, data) => {
    if(err){
        console.log(err);
    }
    else{
        console.log(data);
    }
})