// Import the funciton
const board = require('../callback1')

// Test the function using a callback function
board('abc122dc', (err, data) => {
    if(err){
        console.log(err);
    }
    else{
        console.log(data);
    }
})