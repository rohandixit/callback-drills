// Import the funciton
const callbackFunction = require('../callback2')

// Test the function using a callback function
callbackFunction('abc122dc', (err, data) => {
    if(err){
        console.log(err);
    }
    else{
        console.log(data);
    }
})